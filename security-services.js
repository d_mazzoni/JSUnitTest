;
var User = (function () {

    var Group = (function () {

        //Initialize Array project acl
        var _deleteProjectAcls = [];
        var _addOrModifyAcls = [];
        var _projectsAcl = [];

        var _addedUserIds = [];
        var _deletedUserIds = [];

        var _totalAclCount;

        var Init = function (projectsAcl, groupName, stringsRequired, okButtonText) {

            _projectsAcl = projectsAcl;
            _totalAclCount = projectsAcl.length;        

            $("#add").click(function () {
                $("#listBoxAvail > option:selected").each(function () {
                    $(this).remove().appendTo("#listBoxSel");
                    AddUserId($(this).val());
                });

                //Remove 'No available items' options if exist
                $("#listBoxSel > option:disabled").remove();
            });

            $("#remove").click(function () {
                $("#listBoxSel > option:selected").each(function () {
                    $(this).remove().appendTo("#listBoxAvail");
                    DeleteUserId($(this).val());
                });
            });

            //Add access to a group dialog
            $('#addAccess').click(function (e) {
                e.preventDefault();

                var current = $(this);
                $(current).attr('disabled', 'disabled');

                OpenDialogUserGroupAcl(e, current);
            });
            $('#GroupName').val(groupName);

            $('#user-group-form').submit(function (e) {
                e.preventDefault();
                var buttonSubmit = $(this).find('input[type=submit]');
                var target = $(buttonSubmit).attr('data-url');

                var nameGroup = $('#nameGroup').val();

                var id = $('#Id').val();

                var SendBatch = function (groupId) {
                    if (_addedUserIds.length > 0 || _deletedUserIds.length > 0 || _addOrModifyAcls.length > 0 || _deleteProjectAcls.length > 0 || $('#GroupName').val() != nameGroup) {
                        $('#create-user').attr('disabled', true);
                        var numToSend = 20;
                        var addedUsersToSend = _addedUserIds.splice(0, numToSend);
                        var deletedUsersToSend = _deletedUserIds.splice(0, numToSend);
                        var addOrModifyAclsToSend = _addOrModifyAcls.splice(0, numToSend);
                        var deleteAclsToSend = _deleteProjectAcls.splice(0, numToSend);
                        var deleteAclsProjectIdsToSend = deleteAclsToSend.map(function (acl) { return acl.ProjectId });
                        id = typeof (groupId) != "undefined" ? groupId : id;
                        var postData = JSON.stringify({ idGroup: id, addOrModifyAcls: addOrModifyAclsToSend, deleteAclProjectIds: deleteAclsProjectIdsToSend, name: nameGroup, addUserModels: addedUsersToSend, deleteUserModels: deletedUsersToSend, totalAclCount: _totalAclCount });

                        $.ajax({
                            url: target,
                            data: postData,
                            contentType: 'application/json; charset=utf-8',
                            dataType: 'json',
                            type: 'POST',
                            success: function (result) {
                                if (result.ReturnType === 3) {
                                    if (_addedUserIds.length > 0 || _deletedUserIds.length > 0 || _addOrModifyAcls.length > 0 || _deleteProjectAcls.length > 0)
                                        SendBatch(result.CustomData);
                                    else
                                        ShowSuccessPopup(result.Message);
                                }
                                else {
                                    for (i = 0; i < addedUsersToSend.length; i++) {
                                        _addedUserIds.push(addedUsersToSend[i]);
                                    }
                                    for (i = 0; i < deletedUsersToSend.length; i++) {
                                        _deletedUserIds.push(deletedUsersToSend[i]);
                                    }
                                    for (i = 0; i < deleteAclsToSend.length; i++) {
                                        _deleteProjectAcls.push(deleteAclsToSend[i]);
                                    }
                                    for (i = 0; i < addOrModifyAclsToSend.length; i++) {
                                        _addOrModifyAcls.push({ 'Id': '', 'ProjectId': addOrModifyAclsToSend[i].ProjectId, 'ProjectName': addOrModifyAclsToSend[i].ProjectName, 'AclItems': addOrModifyAclsToSend[i].AclItems });
                                    }
                                    $('#create-user').attr('disabled', false);
                                    $().Cc_ExecuteAjaxData(result);
                                }
                            }
                        });
                    } else if ($('#nameGroup').val() == '') {
                        $('#create-user').attr('disabled', false);
                        var div = '<div id="dialog"><p>Please enter group name.</p></div>';
                        var options = { title: stringsRequired, divId: div, buttonText: okButtonText, autoOpen: false };
                        popup.helptextPopup.init(options);
                        popup.helptextPopup.popupOpenWithoutClick();
                        return false;
                    } else if (_totalAclCount == 0) {
                        $('#create-user').attr('disabled', false);
                        var div = '<div id="dialog"><p>Must add at least one Access Account ACL.</p></div>';
                        var options = { title: stringsRequired, divId: div, buttonText: okButtonText, autoOpen: false };
                        popup.helptextPopup.init(options);
                        popup.helptextPopup.popupOpenWithoutClick();
                        return false;
                    }
                    else {
                        window.location = '/Groups';
                    }
                }
                SendBatch();
            });

            function ShowSuccessPopup(message) {
                var pop = $('<div>' + message + '</div>');
                $(pop).dialog({
                    modal: true,
                    autoOpen: true,
                    width: 'auto',
                    height: 'auto',
                    title: 'Success',
                    dialogClass: 'cc-dialog',
                    buttons: {
                        Ok: {
                            text: 'Ok',
                            'class': 'primary',
                            click: function () {
                                $(pop).dialog('destroy');
                                window.location = '/Groups';
                            }
                        }
                    }
                });
            }

            //Added projects Row - Edit Flow
            for (var i = 0; i < _projectsAcl.length; i++) {
                AddProjectRow(_projectsAcl[i].ProjectId, _projectsAcl[i].ProjectName);
            }

            AddRemoveEmptyTableText();

            $('select[id^="listBoxAvail"]').ListBoxHelper({ UseIdNotName: true, showNoItemMessage: false });
            $('select[id^="listBoxSel"]').ListBoxHelper({ UseIdNotName: true, showNoItemMessage: false }, false);
        }
        	
		var isOpened = false;

        var AddUserId = function (id) {
            _addedUserIds.push(id);
            _deletedUserIds = _deletedUserIds.filter(function (i) { return i !== id });
        }

        var DeleteUserId = function (id) {
            _deletedUserIds.push(id);
            _addedUserIds = _addedUserIds.filter(function (i) { return i !== id });
        }

        var AddOrModifyAclItem = function (acl) {
            var aclToUpdate = _deleteProjectAcls.find(function (currentAcl) { return currentAcl.ProjectId == acl.ProjectId });
            if (aclToUpdate) {
                acl.Id = aclToUpdate.Id;
                _deleteProjectAcls.splice($.inArray(aclToUpdate, _deleteProjectAcls), 1);
            }
            _addOrModifyAcls.push(acl);
        }

        var DeleteAclItem = function (acl) {
            _deleteProjectAcls.push(acl);
            _addOrModifyAcls = _addOrModifyAcls.filter(function (a) { return a.ProjectId !== acl.ProjectId });
            _totalAclCount--;
        }

        var OpenDialogUserGroupAcl = function (e, current) {

            //Avoid multiple clicks
            if (isOpened) return false;
            isOpened = true;

            //Get project id Edit flow
            var projectToEdit = current.data("projectId");
            var projectsAclFiltered = $.grep(_projectsAcl, function (item) { return item.ProjectId == projectToEdit; });
            var projectAclToEdit = projectsAclFiltered.length > 0 ? projectsAclFiltered[0] : null;
            var projectsAclProjectIds = _projectsAcl.map(function (item) {
                return item.ProjectId;
            });

            var aclItemType = $("#SelectedAclItemType").val();
            var urlData = JSON.stringify({ aclItemType: aclItemType, projectsAclProjectIdList: projectsAclProjectIds, aclItemsSelected: projectAclToEdit == null ? null : projectAclToEdit.AclItems, projectId: projectToEdit });

            $.ajax({
                url: '/Admin/UserGroupAcl',
                contentType: 'application/json; charset=utf-8',
                type: 'POST',
                data: urlData,
                success: function (result) {
                    var popup = $('<div>').append(result);
                    $(popup).dialog({
                        autoOpen: true,
                        modal: true,
                        title: 'Access Account-Acl',
                        dialogClass: 'cc-dialog',
                        width: "70%",
                        maxWidth: "768px",
                        height: 650,
                        buttons: {
                            Save: {
                                text: 'Ok',
                                'class': 'primary',
                                click: function () {
                                    var selectedAlc = "";

                                    $('.tab-panels input:checkbox:checked').each(function () {
                                        selectedAlc = selectedAlc + $(this).val() + ',';
                                    });

                                    var selectedProjects = $('#AvailableProjects').val();

                                    //Verify selectedProjects
                                    if (selectedProjects == null || selectedProjects.length == 0) {
                                        $.fn.Cc_CreateOkayPopup("Must select at least one Account", "Error");
                                        return;
                                    }

                                    //Verify acl items selected
                                    if (!selectedAlc) {
                                        $.fn.Cc_CreateOkayPopup("Must select at least one Access-Acl", "Error");
                                        return;
                                    }

                                    //Extra info
                                    var extraInfoProject = '';

                                    $(".tab-panels").children().each(function () {
                                        var countByTab = $(this).find('input:checkbox:checked').size();

                                        if (countByTab > 0) {
                                            var titleTab = $('a[href="#' + $(this).prop('id') + '"]').first().text();
                                            extraInfoProject = extraInfoProject + ' ' + titleTab + '(' + countByTab + ')';
                                        }
                                    });

                                    for (i = 0; i < selectedProjects.length; i++) {
                                        var projectId = selectedProjects[i];
                                        var projectName = $('#AvailableProjects option[value=' + projectId + ']').text();

                                        var currentProjectsFiltered = $.grep(_projectsAcl,
                                            function (item) {
                                                if (item.ProjectId == projectId) {
                                                    item.AclItems = selectedAlc;
                                                    return true;
                                                }
                                                return false;
                                            }
                                        );

                                        var description = projectName + ': ' + extraInfoProject;

                                        if (!UpdateAclItem(projectId, description, selectedAlc)) { //New project seleted
                                            AddProjectRow(projectId, description);
                                            InsertAclItem(projectId, description, selectedAlc);
                                        } else { //Edit project selected
                                            $('#rowTitle' + projectId).text(projectName + ': ' + extraInfoProject);
                                        }
                                    }

                                    AddRemoveEmptyTableText();

                                    $(popup).dialog('destroy');
                                }
                            },
                            Close: {
                                text: 'Close',
                                'class': 'secondary',
                                click: function () {
                                    $(popup).dialog('destroy');
                                }
                            }
                        }
                    }).on('keydown', function (evt) {
                        if (evt.keyCode === $.ui.keyCode.ESCAPE) {
                            $(popup).dialog('destroy');
                        }
                        evt.stopPropagation();
                    });
                },
                complete: function () {
                    isOpened = false;
                }
            });
        }

        var AddProjectRow = function (projectId, projectName) {

            //Create Remove Button
            var buttonRemove = $("<a href='javascript:void(0);' class='delete' id='btnRemove" + projectId + "' ><i class='action align-middle x' title='Delete'></i></a>").click(function (e) {
                e.preventDefault();

                var projectIdToRemove = $(this).data("projectId");

                //remove from array
                var aclToRemove = _projectsAcl.filter(function (item) { return item.ProjectId === projectIdToRemove })[0];
                DeleteAclItem(aclToRemove);
                _projectsAcl = $.grep(_projectsAcl, function (item) { return item.ProjectId !== projectIdToRemove; });

                //remove from div
                $('#li' + projectIdToRemove).remove();
                $('#btnEdit' + projectIdToRemove).remove();
                $('#btnRemove' + projectIdToRemove).remove();
                AddRemoveEmptyTableText();
            }).data("projectId", projectId);


            //Create Modify Button
            var buttonEdit = $("<a href='javascript:void(0);' id='btnEdit" + projectId + "' class= 'edit'  style='text-decoration:none'><i class='action align-middle sprocket' title='Edit' style='margin-right: 5px;'></i></a>")
                .click(function (e) {

                    OpenDialogUserGroupAcl(e, $(this));
                }).data("projectId", projectId);

            var divActions = $("<div style='float:left'></div>");
            $(divActions).append($(buttonEdit));
            $(divActions).append($(buttonRemove));

            var divTitle = $("<div style='background-color:#dedede;float:left; width:100%; padding-top:5px; padding-bottom:5px'></div>");
            $(divTitle).append("<div style='float:left; width:90%; padding-left:20px'><span style='font-weight:bold;' id='rowTitle" + projectId + "'>" + projectName + "</span></div>");
            $(divTitle).append($(divActions));

            var rowProject = $("<li id='li" + projectId + "' ></li>");
            $(rowProject).append($(divTitle));

            $("#accessList").append(rowProject);
        }

        var AddEmptyTableText = function () {

            var divEmpty = $("<div id='divEmpty' class='row center'></div>");
            $(divEmpty).append("<div class='cell'><p>There are no Accounts assigned.</p></div>");
            $("#accessTable").append(divEmpty);
        }

        var AddRemoveEmptyTableText = function () {
            if (_projectsAcl.length == 0) {
                AddEmptyTableText();
            } else {
                $("#divEmpty").remove();
            }
        }

        var UpdateAclItem = function (projectId, projectName, selectedAlc) {
            var update = false;
            var id = '';
            var aclItempProject = $.grep(_projectsAcl,
                function (item) {
                    return (item.ProjectId == projectId);
                });

            if (aclItempProject.length > 0) {// acl already created
                aclItempProject[0].AclItems = selectedAlc;
                aclItempProject[0].ProjectName = projectName;

                id = aclItempProject[0].Id;
                update = true;
            }
            else {
                _totalAclCount++;
                id = null;
            }
            AddOrModifyAclItem({ 'Id': id, 'ProjectId': projectId, 'ProjectName': projectName, 'AclItems': selectedAlc });
            return update;
        }

        var InsertAclItem = function (projectId, projectName, selectedAlc) {
            _projectsAcl.push({ 'Id': '0', 'ProjectId': projectId, 'ProjectName': projectName, 'AclItems': selectedAlc });
        }
		
		vat UnitTest= function()
		{
			QUnit.test( "AddOrModifyAclItem_DeleteAndAddSameProjectAcl_UpdateCurrentAcl", function( assert ) {
			  
			  var acl = { 'Id': '0', 'ProjectId': '1', 'ProjectName': 'TestProject'}};
			  var deleteAcl = { 'Id': '1', 'ProjectId': '1', 'ProjectName': 'TestProject'}};
			  _deleteProjectAcls.push(deleteAcl);
			  
			  AddOrModifyAclItem(acl);
			  
			  var resultAcl = _addOrModifyAcls[0];
			  
			  assert.ok( resultAcl.Id == "1", "Passed!" );
			});
			
			
			QUnit.test( "AddOrModifyAclItem_DeleteAndAddDiffProjectAcl_AddNewAcl", function( assert ) {
			  
			  var acl = { 'Id': '1', 'ProjectId': '1', 'ProjectName': 'TestProject'}};
			  var deleteAcl = { 'Id': '2', 'ProjectId': '2', 'ProjectName': 'TestProject'}};
			  _deleteProjectAcls.push(deleteAcl);
			  
			  AddOrModifyAclItem(acl);			 			 
			  
			  assert.ok( _addOrModifyAcls.length == 1, "Passed!" );
			});
			
			QUnit.test( "AddOrModifyAclItem_AddProjectAcl_AddNewAcl", function( assert ) {
			  
			  var acl = { 'Id': '1', 'ProjectId': '1', 'ProjectName': 'TestProject'}};	
			  
			  AddOrModifyAclItem(acl);			 			 
			  
			  assert.ok( _addOrModifyAcls.length == 1, "Passed!" );
			});
			
			QUnit.test( "AddOrModifyAclItem_AddSameProjectAcl_AddOneAcl", function( assert ) {
			  
			  var acl = { 'Id': '1', 'ProjectId': '1', 'ProjectName': 'TestProject'}};				  
			  AddOrModifyAclItem(acl);
			  
			  AddOrModifyAclItem(acl);			  
			  
			  assert.ok( _addOrModifyAcls.length == 1, "Passed!" );
			});
			
			QUnit.test( "AddOrModifyAclItem_AddSameProjectAcl_AddOneAcl", function( assert ) {
			  
			  var acl = { 'Id': '1', 'ProjectId': '1', 'ProjectName': 'TestProject'}};				  
			  AddOrModifyAclItem(acl);
			  
			  AddOrModifyAclItem(acl);			  
			  
			  assert.ok( _addOrModifyAcls.length == 1, "Passed!" );
			});
		}

        return {
            Init: Init,
			UnitTest:UnitTest
        }
    })();

    return {
        Group: Group
    }

})();